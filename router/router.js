const express = require('express')
const router = express.Router();
const logincontroller = require('../controller/login.controller');
// register routers
const auth = require('../middleware/authen')
router.route('/register').get(logincontroller.register);

// login routers

router.route('/login').get(logincontroller.login);

// detail of login user 

router.route('/detail').get(auth, logincontroller.detail)


module.exports = router