const knex = require('../model/db');
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');

const jwt = require('jsonwebtoken')
dotenv.config({ path: '../.env' })
const User = {
    async register(req, res) {

        const Password = req.body.password;

        const hashpassword = await bcrypt.hash(Password, 10);

        const checkuser = await knex('login').select('EMAIL').where({ EMAIL: req.body.email });

        if (checkuser.length == 0) {
            const registeruser = await knex('login').insert({ EMAIL: req.body.email, PASSWORD: hashpassword });
            res.json({
                success: true,
                status: 201,

            })
        } else {
            res.json({ success: false, message: "User already exists", })
        }
    },
    async login(req, res) {
        const user = await knex('login').where({ EMAIL: req.body.email }).first('*');
        if (user) {
            const psswordcheck = await bcrypt.compare(req.body.password, user.PASSWORD);
            const email = req.body.email;
            if (psswordcheck) {
                const token = jwt.sign({ email }, process.env.SERCET_KEY, { expiresIn: '1h' })
                res.json({ success: true, message: "Login Successfully", token }
                );
            } else {
                res.json({ success: false, message: "Enter email and password" });
            }
        } else {
            res.json({ success: false, message: "login false" });
        }

    },
    async detail(req, res) {

        const userdata = await knex('login').select('*').where({ EMAIL: req.user.email })
        res.json({
            success: true,
            message: "Token is verfiy",
            userdata
        })
    }

}
module.exports = User 