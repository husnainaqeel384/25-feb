const express = require('express')
const bodyParser = require('body-Parser')
const app = express();

const dotenv = require('dotenv');
dotenv.config({ path: './.env' })

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const routes = require('./router/router')
app.use('/v1', routes)


app.listen(process.env.PORT, () => {
    console.log(`Server is Running at ${process.env.PORT}`);
})